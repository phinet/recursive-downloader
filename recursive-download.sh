#!/bin/bash

chunk_size=100
concurrent_downloads=5
connections_per_download=5

excluded_types=()
excluded_names=()
excluded_patterns=()

included_types=()
included_names=()
included_patterns=()

download_paths=()

crawl () {
	cd /tmp

	for url in "$@"; do
		wget --spider -r --no-parent -e robots=off "$url" 2>&1
	done
}

spinnerparts=(
	'[=====     ]'
	'[ =====    ]'
	'[  =====   ]'
	'[   =====  ]'
	'[    ===== ]'
	'[     =====]'
	'[=     ====]'
	'[==     ===]'
	'[===     ==]'
	'[====     =]'
)
spinnerindex=0
spinner () {
	>&2 printf "${spinnerparts[$spinnerindex]}\r"

	((spinnerindex+=1))

	if [ "$spinnerindex" -ge "${#spinnerparts[@]}" ]; then
		spinnerindex=0
	fi
}

parse () {
	output_buffer=

	while read -r line; do
		url=
		size=
		type=

		if [[ "$line" == "--"* ]]; then # url section
			url="$line"
			url="${url##* }" # extract url portion (--date-- url -> url)
			url="${url%\?*}" # remove querystring (url?query -> url)

			if [[ "$url" == *"/" ]]; then # filter directories
				( spinner )

				url=
			fi

			if [[ "$url" ]]; then
				type="url"
			fi
		elif [[ "$line" == "Length:"* ]]; then # size section
			size=$line
			size=${size#* } # remove length prefix (Length: xxx yyy -> xxx yyy)
			size=${size%% *} # isolate size in bytes (size other -> size)

			if [[ "$size" == "unspecified" ]]; then # filter directories
				size=-1
			fi

			type="size"
		fi

		if [[ "$type" == "url" ]]; then
			output_buffer="$url"

			last_was_a_url=true
		elif [[ "$type" == "size" && "$last_was_a_url" ]]; then
			output_buffer="$output_buffer $size"

			echo "$output_buffer"

			last_was_a_url=
		fi
	done
}

number_format () {
	input=$(numfmt --to=si --format=%.5f <<< "$1")

	numeric=$(perl -pe "s/\D+$//g" <<< "$input")

	size=$(( ${#numeric} - ${#input} ))

	suffix=${input:($size)}

	printf "%.5s %s" "$numeric" "$suffix"
}

print_status_and_download () {

	files="$1"
	size="$(number_format $2)"

	total_files="$3"
	total_size="$(number_format $4)"

	download_data="$5"

	echo
	echo "     -----"
	echo
	echo "Number of files to download: $files"
	echo "Size estimate: $size"
	echo
	echo "Total files to date: $total_files"
	echo "Total size estimate: $total_size"
	echo
	echo "     -----"

	aria2c -i - -c -m 0 -j "$concurrent_downloads" -x "$connections_per_download" <<< "$download_data"
}

download () {
	echo "Crawling. Please be patient"

	download_data=

	total_number_of_files=0
	total_size=0

	current_number_of_files=0
	current_size=0

	while read -r line; do
		url="${line% *}"
		size="${line#* }"

		out=$(tr "<>:\"|*" + <<< "${url##*://}")
		out=$(urldecode <<< "$out")

		total_number_of_files=$(bc <<< "$total_number_of_files + 1")
		current_number_of_files=$(bc <<< "$current_number_of_files + 1")

		if (( "$size" > 0 )); then # track total size to download
			total_size=$(bc <<< "$total_size + $size")
			current_size=$(bc <<< "$current_size + $size")
		fi

		download_data="$download_data""$url"$'\n'
		download_data="$download_data""  out=$out"$'\n'

		echo $(urldecode "$url")

		if (( "$current_number_of_files" >= "$chunk_size" )); then

			( print_status_and_download "$current_number_of_files" "$current_size" "$total_number_of_files" "$total_size" "$download_data" )

			download_data=
			current_number_of_files=0
			current_size=0
		fi
	done

	if (( "$current_number_of_files" > 0 )); then
		( print_status_and_download "$current_number_of_files" "$current_size" "$total_number_of_files" "$total_size" "$download_data" )
	fi
}

filter () {
	while read -r line; do
		url="${line% *}"
		size="${line#* }"

		path=$(urldecode $url)

		filename=$(basename -- "$path")
		filename="${filename%.*}"

		# skip urls with filetypes specified
		for type in "${excluded_types[@]}"; do
			if [[ $path == *$type ]]; then
				continue 2
			fi
		done

		# skip filenames matching pattern
		for name in "${excluded_names[@]}"; do
			if [[ $filename =~ $name ]]; then
				continue 2
			fi
		done

		# skip urls matching pattern
		for pattern in "${excluded_patterns[@]}"; do
			if [[ $path =~ $pattern ]]; then
				continue 2
			fi
		done

		# only allows filetypes specified
		if [ "${#included_types[@]}" -gt 0 ]; then
			found=

			for type in "${included_types[@]}"; do
				if [[ $path == *$type ]]; then
					found=1
					break
				fi
			done

			# not found
			[ -z $found ] && continue
		fi

		# only allows filenames matching pattern
		if [ "${#included_names[@]}" -gt 0 ]; then
			found=

			for name in "${included_names[@]}"; do
				if [[ $filename =~ $name ]]; then
					found=1
					break
				fi
			done

			# not found
			[ -z $found ] && continue
		fi

		# only allows urls matching pattern
		if [ "${#included_patterns[@]}" -gt 0 ]; then
			found=

			for pattern in "${included_patterns[@]}"; do
				if [[ $path =~ $pattern ]]; then
					found=1
					break
				fi
			done

			# not found
			[ -z $found ] && continue
		fi

		# if we've gotten to this point, the file is okay to download
		echo $line
	done
}

helptext () {
	echo "Usage"

	exit 1
}

read_arguments () {
	for i in "$@"; do
		case $i in

		-h|--help)
			helptext
		;;

		--chunksize=*)
			chunk_size="${i#*=}"
			shift # past argument=value
		;;

		--downloads=*)
			concurrent_downloads="${i#*=}"
			shift # past argument=value
		;;

		--connections=*)
			connections_per_download="${i#*=}"
			shift # past argument=value
		;;

		-f=*|--typeexclude=*)
			excluded_types+=("${i#*=}")
			shift # past argument=value
		;;
		-F=*|--typeinclude=*)
			included_types+=("${i#*=}")
			shift # past argument=value
		;;

		-n=*|--nameexclude=*)
			excluded_filetypes+=("${i#*=}")
			shift # past argument=value
		;;
		-N=*|--nameinclude=*)
			included_names+=("${i#*=}")
			shift # past argument=value
		;;

		-p=*|--patternexclude=*)
			excluded_patterns+=("${i#*=}")
			shift # past argument=value
		;;
		-P=*|--patterninclude=*)
			included_patterns+=("${i#*=}")
			shift # past argument=value
		;;

		*)
			download_paths+=("${i#*=}")
			# unknown option (url)
		;;

		esac
	done
}

print_list () {
	printf '    %s\n' $(urldecode <<< "$@")
	echo
}

print_if_not_empty () {
	if [ $# -gt 1 ]; then
		printf "%s\n\n" "$1"
		shift
		print_list "$@"
	fi
}

validate () {
	for path in "${download_paths[@]}"; do
		if [[ $path != */ ]]; then
			read -p "'$path' has no trailing slash, this may cause problems." okay

			if [ "$okay" != "Y" ]; then
				exit 1
			fi
		fi
	done
}

echo "Recursive download manager"
echo

read_arguments "$@"

print_list "${download_paths[@]}"

print_if_not_empty "Excluding filetypes:" "${excluded_types[@]}"
print_if_not_empty "Excluding filenames matching:" "${excluded_names[@]}"
print_if_not_empty "Excluding paths matching:" "${excluded_patterns[@]}"

print_if_not_empty "Only downloading filetypes:" "${included_types[@]}"
print_if_not_empty "Only downloading filenames matching:" "${included_names[@]}"
print_if_not_empty "Only downloading paths matching:" "${included_patterns[@]}"

echo "Downloading in chunks of $chunk_size, $concurrent_downloads files at a time, with $connections_per_download connections per download"
echo

validate

crawl "${download_paths[@]}" | parse | filter | download